import "./App.scss";
import Header from "./components/header";
import { Route, Routes } from "react-router-dom";
import { Cart, Favorites, Home } from "./pages";
import { useEffect, useState } from "react";
import { getProductsAsync } from "./redux/actions/products";
import { useDispatch, useSelector } from "react-redux";
import Modal from "./components/modal";
import { closeModal } from "./redux/actions/modal";
import { ThemeContext } from "./context/themeContext";

function App() {
  const dispatch = useDispatch();
  const { modalIsOpen, modalData } = useSelector((state) => state.modal);
  const [theme, setTheme] = useState("cards");

  useEffect(() => {
    dispatch(getProductsAsync());
  }, [dispatch]);
  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/favorites" element={<Favorites />} />
      </Routes>
      {modalIsOpen && (
        <Modal
          header={modalData.header}
          text={modalData.text}
          actions={modalData.actions}
          closeButton={modalData.closeButton}
          closeButtonHandler={() =>
            function () {
              dispatch(closeModal());
            }
          }
        />
      )}
    </ThemeContext.Provider>
  );
}

export default App;
