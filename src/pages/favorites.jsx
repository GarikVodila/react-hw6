import NoProducts from "../components/noProducts";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import PageHeader from "../components/pageHeader";
import ProductsWrapper from "../components/productsWrapper";

export function Favorites() {
  const { inFavorites: favorites } = useSelector((state) => state.favorites);

  return (
    <>
      <main className="main">
        <section className="container">
          {favorites.length === 0 ? (
            <NoProducts target="favorites" />
          ) : (
            <>
              <PageHeader />
              <ProductsWrapper filter="favorites" />
            </>
          )}
        </section>
      </main>
    </>
  );
}

Favorites.propTypes = {
  productList: PropTypes.arrayOf(PropTypes.object),
  onLocalStorageChanged: PropTypes.func,
};
