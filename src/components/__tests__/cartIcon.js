import CartIcon from "../cartIcon";
import { render, screen, cleanup } from "@testing-library/react";
import renderer from "react-test-renderer";

afterEach(cleanup);

test("snapshot cart icon", () => {
  const cartIconProps = {
    className: "cartIcon",
    color: "red",
  };

  const tree = renderer.create(<CartIcon {...cartIconProps} />).toJSON();
  expect(tree).toMatchSnapshot();
});
