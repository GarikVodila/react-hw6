import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import ProductsWrapper from "../productsWrapper";
import { middleware } from "../../redux/middleware";
import Button from "../button";

const mockStore = configureStore(middleware);
let store;

beforeEach(() => {
  store = mockStore({
    cart: {
      inCart: [26564],
    },
    favorites: {
      inFavorites: [26565],
    },
    products: {
      products: [
        {
          name: "Apple iPhone 14 128GB (e-Sim)",
          price: 829,
          imgURL: "./products-img/26564.png",
          article: 26564,
          color: "Midnight",
        },
        {
          name: "Apple iPhone 14 128GB (MPUR3)",
          price: 879,
          imgURL: "./products-img/26565.png",
          article: 26565,
          color: "Starlight",
        },
      ],
    },
  });
  cleanup();
});

test("should render products with add to & delete from cart btns", () => {
  render(
    <Provider store={store}>
      <ProductsWrapper />
    </Provider>
  );

  const addToCartBtn = screen.getByText("Add to cart");
  expect(addToCartBtn).toBeInTheDocument();

  const deleteFromCartBtn = screen.getByText("Delete from cart");
  expect(deleteFromCartBtn).toBeInTheDocument();
});

test("should add to favorites on click 'unfilled star icon'", () => {
  render(
    <Provider store={store}>
      <ProductsWrapper />
    </Provider>
  );

  const addToFavsBtn = screen.getByTestId("addToFavs");
  expect(addToFavsBtn).toBeInTheDocument();
  fireEvent.click(addToFavsBtn);

  const actions = store.getActions();
  expect(actions).toEqual([
    {
      type: "Favorites - ADD_TO_FAVORITES",
      payload: { id: 26564 },
    },
  ]);
});

test("should delete from favorites on click 'filled star icon'", () => {
  render(
    <Provider store={store}>
      <ProductsWrapper />
    </Provider>
  );

  const deleteFromFavsBtn = screen.getByTestId("deleteFromFavs");
  expect(deleteFromFavsBtn).toBeInTheDocument();
  fireEvent.click(deleteFromFavsBtn);

  const actions = store.getActions();
  expect(actions).toEqual([
    {
      type: "Favorites - DELETE_FROM_FAVORITES",
      payload: { id: 26565 },
    },
  ]);
});
