import { cleanup } from "@testing-library/react";
import renderer from "react-test-renderer";
import StarIcon from "../starIcon";

afterEach(cleanup);

test("snapshot star icon", () => {
  const starIconProps = {
    className: "starIcon",
    onClick: function () {},
    article: 123456,
    fill: true,
    color: "blue",
  };

  const tree = renderer.create(<StarIcon {...starIconProps} />).toJSON();
  expect(tree).toMatchSnapshot();
});
