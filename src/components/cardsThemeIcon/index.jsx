import PropTypes from "prop-types";

export default function CardsThemeIcon(props) {
  return (
    <svg
      onClick={() => props.clickHandler()}
      className={props.className}
      width="48"
      height="48"
      viewBox="0 0 48 48"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="8.5"
        y="8.5"
        width="13.5"
        height="13"
        rx="1.5"
        stroke="#282C34"
      />
      <rect x="26" y="8.5" width="13.5" height="13" rx="1.5" stroke="#282C34" />
      <rect
        x="8.5"
        y="26.5"
        width="13.5"
        height="13"
        rx="1.5"
        stroke="#282C34"
      />
      <rect
        x="26"
        y="26.5"
        width="13.5"
        height="13"
        rx="6.5"
        stroke="#282C34"
      />
    </svg>
  );
}

CardsThemeIcon.propTypes = {
  className: PropTypes.string,
};
