import TableThemeIcon from "../tableThemeIcon";
import CardsThemeIcon from "../cardsThemeIcon";
import { useContext } from "react";
import { ThemeContext } from "../../context/themeContext";

export default function PageHeader() {
  const { theme, setTheme } = useContext(ThemeContext);

  return (
    <>
      <h2 className="title">Choose your iPhone!)</h2>
      <div className="themeWrap">
        <span>Switch view to:</span>
        {theme === "cards" ? (
          <TableThemeIcon
            className="themeIcon"
            clickHandler={() => setTheme("table")}
          />
        ) : (
          <CardsThemeIcon
            className="themeIcon"
            clickHandler={() => setTheme("cards")}
          />
        )}
      </div>
    </>
  );
}
