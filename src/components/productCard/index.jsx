import styles from "./ProductCard.module.scss";
import StarIcon from "../starIcon";
import PropTypes from "prop-types";
import ToggleCartButton from "../toggleCartButton";

export default function ProductCard(props) {
  return (
    <>
      <div className={styles.cardItem}>
        <img className={styles.img} src={props.imgURL} alt="photo" />
        <div className={styles.header}>
          <h2 className={styles.title}>{props.name}</h2>
          <StarIcon
            onClick={() =>
              props.setIsInFavorite(props.isFavorite, props.article)
            }
            article={props.article}
            fill={props.isFavorite}
            className={styles.favoriteIcon}
          />
        </div>
        <div className={styles.description}>
          <div className={styles.color}>
            <span className={styles.label}>Color:</span>
            <span className={styles.value}>{props.color}</span>
          </div>
          <div className={styles.article}>
            <span className={styles.label}>Article:</span>
            <span data-testid="article" className={styles.value}>
              {props.article}
            </span>
          </div>
        </div>
        <div className={styles.priceWrap}>
          <p className={styles.price}>{props.price}$</p>
          <ToggleCartButton
            inCart={props.inCart}
            createModal={props.createModal}
            article={props.article}
          />
        </div>
      </div>
    </>
  );
}

ProductCard.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  imgURL: PropTypes.string,
  article: PropTypes.number,
  color: PropTypes.string,
  isFavorite: PropTypes.bool,
  inCart: PropTypes.bool,
  setIsInFavorite: function () {},
  createModal: function () {},
};

ProductCard.defaultProps = {
  isFavorite: false,
  inCart: false,
};
