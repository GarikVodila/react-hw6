import PropTypes from "prop-types";

export default function TableThemeIcon(props) {
  return (
    <svg
      onClick={() => props.clickHandler()}
      className={props.className}
      width="48"
      height="48"
      viewBox="0 0 48 48"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect x="4.5" y="8.5" width="39" height="13" rx="1.5" stroke="#282C34" />
      <rect x="4.5" y="26.5" width="39" height="13" rx="1.5" stroke="#282C34" />
    </svg>
  );
}

TableThemeIcon.propTypes = {
  className: PropTypes.string,
};
