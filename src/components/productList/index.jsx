import styles from "./ProductList.module.scss";
import ProductCard from "../productCard";
import PropTypes from "prop-types";
export default function ProductList(props) {
  return (
    <ul data-testid="products" className={styles.productList}>
      {props.products.map((product) => (
        <ProductCard
          key={product.article}
          {...product}
          isFavorite={props.favorites.includes(product.article)}
          inCart={props.cart.includes(product.article)}
          setIsInFavorite={props.setIsInFavorite}
          createModal={props.createModal}
        />
      ))}
    </ul>
  );
}

ProductList.propTypes = {
  products: PropTypes.array,
  cart: PropTypes.array,
  favorites: PropTypes.array,
  setIsInFavorite: function () {},
  createModal: function () {},
};

ProductList.defaultProps = {
  cart: [],
  favorites: [],
};
