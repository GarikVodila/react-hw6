import styles from "./ProductRow.module.scss";
import PropTypes from "prop-types";
import StarIcon from "../starIcon";
import ToggleCartButton from "../toggleCartButton";

export default function ProductRow(props) {
  return (
    <tr className={styles.row}>
      <td className={styles.col}>
        <img className={styles.img} src={props.imgURL} alt="photo" />
      </td>
      <td className={styles.col}>
        <h2 className={styles.title}>{props.name}</h2>
      </td>
      <td className={styles.col}>
        <span className={styles.color}>{props.color}</span>
      </td>
      <td className={styles.col}>
        <span className={styles.article}>{props.article}</span>
      </td>
      <td className={styles.col}>
        <p className={styles.price}>{props.price}$</p>
      </td>
      <td className={styles.col}>
        <StarIcon
          onClick={() => props.setIsInFavorite(props.isFavorite, props.article)}
          article={props.article}
          fill={props.isFavorite}
          className={styles.favoriteIcon}
        />
      </td>
      <td className={styles.col}>
        <ToggleCartButton
          inCart={props.inCart}
          createModal={props.createModal}
          article={props.article}
        />
      </td>
    </tr>
  );
}

ProductRow.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  imgURL: PropTypes.string,
  article: PropTypes.number,
  color: PropTypes.string,
  isFavorite: PropTypes.bool,
  inCart: PropTypes.bool,
  setIsInFavorite: function () {},
  createModal: function () {},
};

ProductRow.defaultProps = {
  isFavorite: false,
  inCart: false,
};
