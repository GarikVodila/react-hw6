import { useFormik } from "formik";
import styles from "./BuyProductForm.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { clearCart } from "../../redux/actions/cart";
import { buyProductSchema } from "../../schemas";
import { PatternFormat } from "react-number-format";
import { getItemFromLocalStorage } from "../../utility/localStorage";

export default function BuyProductForm() {
  const { products } = useSelector((state) => state.products);
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      age: "",
      address: "",
      phone: "",
    },
    validationSchema: buyProductSchema,
    onSubmit: (values, { resetForm }) => {
      resetForm();
      const inCart = getItemFromLocalStorage("productInCart");
      const { firstName, lastName, age, address, phone } = values;

      console.log("Order information");
      console.table({
        ["First name"]: `${firstName}`,
        ["Last name"]: `${lastName}`,
        ["Age"]: `${age}`,
        ["Address"]: `${address}`,
        ["Phone number"]: `${phone}`,
      });
      console.log("Product list:");

      products
        .filter((product) => inCart.includes(product.article))
        .map(({ name, price, color, article }) => {
          console.table({
            ["Product name"]: `${name}`,
            ["Price"]: `${price}`,
            ["Color"]: `${color}`,
            ["Article"]: `${article}`,
          });
        });
      dispatch(clearCart("productInCart"));
    },
  });
  return (
    <div className={styles.wrap}>
      <form onSubmit={formik.handleSubmit} className={styles.form}>
        <div className={styles.formGroup}>
          <label htmlFor="firstName" className={styles.label}>
            First name
          </label>
          <input
            className={styles.input}
            type="text"
            id="firstName"
            name="firstName"
            value={formik.values.firstName}
            onChange={formik.handleChange}
          />
          {formik.errors.firstName && formik.touched.firstName ? (
            <span className={styles.error}>{formik.errors.firstName}</span>
          ) : null}
        </div>
        <div className={styles.formGroup}>
          <label htmlFor="lastName" className={styles.label}>
            Last name
          </label>
          <input
            className={styles.input}
            type="text"
            id="lastName"
            name="lastName"
            value={formik.values.lastName}
            onChange={formik.handleChange}
          />
          {formik.errors.lastName && formik.touched.lastName ? (
            <span className={styles.error}>{formik.errors.lastName}</span>
          ) : null}
        </div>
        <div className={styles.formGroup}>
          <label htmlFor="age" className={styles.label}>
            Age
          </label>
          <input
            className={styles.input}
            type="number"
            id="age"
            name="age"
            value={formik.values.age}
            onChange={formik.handleChange}
          />
          {formik.errors.age && formik.touched.age ? (
            <span className={styles.error}>{formik.errors.age}</span>
          ) : null}
        </div>
        <div className={styles.formGroup}>
          <label htmlFor="address" className={styles.label}>
            Address
          </label>
          <input
            className={styles.input}
            type="text"
            id="address"
            name="address"
            value={formik.values.address}
            onChange={formik.handleChange}
          />
          {formik.errors.address && formik.touched.address ? (
            <span className={styles.error}>{formik.errors.address}</span>
          ) : null}
        </div>
        <div className={styles.formGroup}>
          <label htmlFor="phone" className={styles.label}>
            Phone
          </label>
          <PatternFormat
            className={styles.input}
            type="text"
            id="phone"
            name="phone"
            value={formik.values.phone}
            onChange={formik.handleChange}
            format="+38 (0##) #### ###"
            allowEmptyFormatting
            mask="_"
          />
          {formik.errors.phone && formik.touched.phone ? (
            <span className={styles.error}>{formik.errors.phone}</span>
          ) : null}
        </div>
        <button type="submit" className={styles.button}>
          Checkout
        </button>
      </form>
    </div>
  );
}
