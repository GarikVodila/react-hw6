import { productsTypes } from "../types";

const initialState = {
  products: [],
  error: "",
  loaded: false,
};

export function productsReducer(state = initialState, action) {
  switch (action.type) {
    case productsTypes.GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.payload.data,
      };
    case productsTypes.GET_PRODUCTS_REQUESTED:
      return {
        ...state,
        loaded: !state.loaded,
      };
    case productsTypes.GET_PRODUCTS_ERROR:
      return {
        ...state,
        error: action.payload.message,
      };
    default:
      return state;
  }
}
